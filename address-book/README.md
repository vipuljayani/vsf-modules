# Customer address book extension for Vue Storefront
Customer address book extension for [vue-storefront](https://github.com/DivanteLtd/vue-storefront), by [Aureate Labs](https://aureatelabs.com)
> This extension is used for manage customer multiple addresses.

![Customer Address Book](./docs/address.png)

## Installation

By hand (preferer):
```
git clone https://github.com/aureatelabs/vsf-address-book.git ./vue-storefront/src/modules/address-book
```

Add the following config to your `config/local.json` 
```
"addressbook": {
    "create_endpoint": "/api/ext/address-book/customers/{{customerId}}",
    "remove_endpoint": "/api/ext/address-book/addresses/{{addressId}}"
},
```

## Registration the customer address book extension

Add script import to `./src/modules/client.ts`
```
...
import { AddressBookModule } from './address-book'

export function registerClientModules () {
    ...,
    registerModule(AddressBookModule)
}
```

Need to add component instance after shipping information view code.Import customer address component in MyShippingDetails.vue
```
...
    </div>

    <CustomerAddress />
  </div>
</template>

<script>

import CustomerAddress from 'src/modules/address-book/components/CustomerAddress'
export default {
  components: {
    ...,
    CustomerAddress
  },
...
```

![Customer Address Book](./docs/shipping.png)

## Add ShippingAddressList in Checkout => Shipping.vue
Need to ShippingAddressList component in Shipping.vue and also add some codes line in script
```
...
        <i class="material-icons cl-tertiary">edit</i>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<shipping-address-list :is-active="isActive" />
<div class="row pl20" v-if="isActive">
  <div class="hidden-xs col-sm-2 col-md-1" />
    <div class="col-xs-11 col-sm-9 col-md-10">
      <div class="row" v-if="visibleShippingAddress">

<script>
import ShippingAddressList from 'src/modules/address-book/components/ShippingAddressList'      
export default {
  components: {
    ...,
    ShippingAddressList
  },
  data () {
    return {
      visibleShippingAddress: false
    }
  },
  beforeMount () {
    this.$bus.$on('visible-shipping-address-form', (params) => {
      this.visibleShippingAddress = params;
      if (this.visibleShippingAddress) {
        this.shipping = {}
      }
    })
  }
...
```

![Customer Address Book](./docs/billing.png)

## Add BillingAddressList in Checkout => Payment.vue
Need to BillingAddressList component in Payment.vue and also add some codes line in script
```
...
        <i class="material-icons cl-tertiary">edit</i>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
<billing-address-list :is-active="isActive" />
    <div class="row pl20" v-if="isActive">
      <div class="hidden-xs col-sm-2 col-md-1" />
      <div class="col-xs-11 col-sm-9 col-md-10">
        <div class="row">
          <div class="row" v-if="visibleBillingAddress"> // add to div and close to before generateInvoice checkbox

<script>
import BillingAddressList from 'src/modules/address-book/components/BillingAddressList'      
export default {
  components: {
    ...,
    BillingAddressList
  },
  data () {
    return {
      visibleBillingAddress: false
    }
  },
  beforeMount () {
    this.$bus.$on('visible-billing-address-form', (params) => {
      this.visibleBillingAddress = params;
      if (this.visibleBillingAddress) {
        this.payment = {}
      }
    })
  }
...
```

## Customer address book API extension

Install additional extension for `vue-storefront-api:`

```
$ cp -f ./API/address-book ../vue-storefront-api/src/api/extensions/
```

Add the following config to your `./vue-storefront-api/config/local.json` for Registration
```
"registeredExtensions": [
  ...
  "mail-service",
  "address-book"
],
```
## License

This project is licensed under the [MIT License](https://github.com/aureatelabs/vsf-address-book/blob/master/LICENSE.txt)
