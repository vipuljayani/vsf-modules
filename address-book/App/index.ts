import { addressBookModule } from './store'
import { StorefrontModule } from '@vue-storefront/core/lib/modules';

export const AddressBookModule: StorefrontModule = function ({ store }) {
  store.registerModule('address-book', addressBookModule)
}
